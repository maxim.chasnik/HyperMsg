﻿namespace HyperMsg;

public delegate void Deserializer(IBufferReader bufferReader, IDispatcher dispatcher);
